
import {Configuration} from 'webpack';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import path from 'path';

import mainConfig from '../webpack.config';
import {styleRule} from '../rules';


const config: Configuration = {

    ...mainConfig,

    entry: path.resolve(__dirname, '..', '..', 'src', 'index.prod.tsx'),

    output: {
        ...mainConfig.output,
        filename: 'bundle.min.js'
    },

    mode: 'production',

    module: {
        ...mainConfig.module,
        rules: [
            ...mainConfig.module!.rules,
            styleRule('production'),
        ]
    },

    optimization: {
        minimize: true,
        minimizer: [
            new CssMinimizerPlugin()
        ]
    }
}


export default config;
