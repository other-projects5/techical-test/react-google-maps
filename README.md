# GoogleMaps test

## About the project

This project is a technical test using ReactJS framework
with Typescript. Between it's main features you can find:

- Typescript
- React
- Redux (Thunk)
- SCSS
- Storybook **(Work in progress)**
- Jest

## Directory architecture

```
.
├── public
├── src
│   ├── components
│   ├── config - Some config params.
│   ├── models
│   ├── services
│   ├── store
│   │   ├── actions
│   │   ├── reducers
│   │   └── typings - Helpful typings used for redux thunk
│   ├── styles - Basic and shared styles, variables and mixins.
│   ├── index.ejs - index.html template
│   └── index.tsx - React Dom render (development and production in diferent files, .dev.tsx and .prod.tsx)
└── test
    ├── specs - Tests for components, utils, redux, ...
    └── stories - Isolated Components
    └── utils
```

## Development

For development use the following scripts:

- `npm run dev:webpack` will launch a development environment at `http://localhost:3000`, served using 
`webpack-dev-server` with **hot module replacement** configured.

## Testing
 
Inside the project you will find `test.ts` files inside "test/specs" folder.

For testing use the following script:

- `npm run test:jest` will run all test suites.

## Build and Deploy

To build the app execute this script `npm run build`. This will create everything needed to serve the app inside `public` folder.
