
import {Action, Dispatch} from '../types/redux';
import {ADD_MARKER} from '../types/googleMapsTypes';

import {Coordinates} from '../../models';
import {store} from '../store';


export const addMarkerToMap = (map_id: string, coordinates: Coordinates[]): Action => {

    return (dispatch: Dispatch) => {

        const state = store.getState();

        let markers = state.googleMaps.markers;

        if(!!markers.find((m) => m.map_id === map_id)) {

            markers = markers.map((m) => {

                return m.map_id === map_id ?
                    {...m, coordinates: [...m.coordinates, ...coordinates]}
                    : m;
            });

        } else {
            markers.push({
                map_id,
                coordinates
            });
        }


        dispatch({
            type: ADD_MARKER,
            payload: markers
        });
    };
}
