
import {IAppState} from './IAppState';


const state: IAppState = {

    googleMaps: {
        markers: []
    }

};


export const initialState = process.env.NODE_ENV === 'development' ? {
    ...state
} : state;
