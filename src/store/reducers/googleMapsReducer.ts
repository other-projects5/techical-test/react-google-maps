
import {Reducer} from 'redux';

import {IGoogleMapsState} from '../IAppState';
import {initialState} from '../initialState';
import {ADD_MARKER} from '../types/googleMapsTypes';


export const googleMapsReducer: Reducer<IGoogleMapsState> = (state: IGoogleMapsState = initialState.googleMaps, {type, payload}) => {

    switch (type) {

        case ADD_MARKER:

            return {
                ...state,
                markers: payload
            };

        default:
            return state;
    }
}
