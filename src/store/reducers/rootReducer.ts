
import {combineReducers, Reducer} from 'redux';

import {IAppState} from '../IAppState';
import {googleMapsReducer} from './googleMapsReducer';


export const rootReducer: Reducer<IAppState> = combineReducers({
    googleMaps: googleMapsReducer
});
