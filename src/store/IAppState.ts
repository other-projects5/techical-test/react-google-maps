
import {Coordinates} from '../models';


export interface IAppState {

    googleMaps: IGoogleMapsState;

}

export interface IGoogleMapsState {

    markers: IMarkerState[];

}

export interface IMarkerState {

    map_id: string;

    coordinates: Coordinates[];

}
