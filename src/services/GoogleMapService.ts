
import {RefObject} from 'react';

import {Coordinates, PredictionConfig} from '../models';
import Map = google.maps.Map;
import PlacesServiceStatus = google.maps.places.PlacesServiceStatus;
import AutocompletePrediction = google.maps.places.AutocompletePrediction;
import PlaceResult = google.maps.places.PlaceResult;


class GoogleMapServiceClass {

    private Map: any;
    private Marker: any;
    private LatLng: any;
    private Session: any;
    private PlacesService: any;

    private token: any;
    private marker: any;
    private autocomplete: any;
    private placesService: any;


    public constructor() {
        this.initLoadProps();
    }

    private initLoadProps(): void {

        if(typeof google !== 'undefined') {

            let {AutocompleteService, AutocompleteSessionToken, PlacesService} = google.maps.places;

            this.Map = google.maps.Map;
            this.Marker = google.maps.Marker;
            this.LatLng = google.maps.LatLng;

            this.Session = AutocompleteSessionToken;
            this.token = new this.Session();
            this.PlacesService = PlacesService;
            this.autocomplete = new AutocompleteService();
        }
    };

    public initMap(element: RefObject<HTMLDivElement>, coordinates: Coordinates, zoom: number): Promise<Map> {

        return new Promise<Map>((resolve) => {

            let map = new this.Map(
                element.current,
                {
                    center: coordinates,
                    zoom,
                    disableDefaultUI: true
                });

            this.placesService = new this.PlacesService(map);
            resolve(map);
        });
    }

    public getQueryPredictions(query: string, config: PredictionConfig): Promise<AutocompletePrediction[]> {

        return new Promise<AutocompletePrediction[]>((resolve) => {

            this.autocomplete.getPlacePredictions({
                input: query,
                sessionToken: this.token,
                // types: [config.type],
                radius: config.radius,
                location: config.coordinates ? new this.LatLng(config.coordinates) : undefined
            }, (predictions: AutocompletePrediction[]) => resolve(predictions || []))
        });
    };

    public getPredictionDetails(prediction: AutocompletePrediction): Promise<PlaceResult> {

        return new Promise<PlaceResult>((resolve) => {

            this.placesService.getDetails({
                placeId: prediction.place_id,
                sessionToken: this.token,
                fields: ['geometry', 'formatted_address', 'place_id']
            }, (details: PlaceResult, status: PlacesServiceStatus) => {

                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    resolve(details);
                } else {
                    // TODO: Need to handle error
                    console.log('Google error');
                }

                this.token = new this.Session();
            });
        });
    };

    public addMarker(map: Map, coordinates: Coordinates): void {
        this.marker = new this.Marker({map, position: coordinates});
    }

    public removeMarker(): void {

        if(this.marker) {
            this.marker.setMap(null);
        }
    }
}


export const GoogleMapService = new GoogleMapServiceClass();
