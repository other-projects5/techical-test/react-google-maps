
import {Coordinates} from './Coordinates';
import {PlacesType} from './PlacesTypes';


export interface PredictionConfig {

    radius: number;

    coordinates: Coordinates;

    countries?: string[];

    type?: PlacesType;

}
