
import * as React from 'react';
import * as ReactDom from 'react-dom';
import {AppContainer} from 'react-hot-loader';

import {App} from './components';


declare let module: {hot: any};

const root = document.getElementById('root');

ReactDom.render(
    <AppContainer>
        <App />
    </AppContainer>,
    root
);


if(module.hot) {

    module.hot.accept('./components/App', () => {

        const NewApp = require('./components/App').App;

        ReactDom.render(
            <AppContainer>
                <NewApp />
            </AppContainer>,
            root
        );
    });
}

