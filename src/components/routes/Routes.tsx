
import * as React from 'react';
import {FunctionComponent} from 'react';
import {Switch, Route} from 'react-router-dom';

import {HomePage} from '../pages';

import {routes} from '../../config/routes';

import '../../styles/main.scss';


export const Routes: FunctionComponent = () => {

    return (
        <Switch>
            <Route exact name="home" path={routes.home} component={HomePage} />
        </Switch>
    );
}
