
import * as React from 'react';
import {FunctionComponent, useState} from 'react';

import {Container, Row, Col} from '../../../__lib__/react-components/layout';

import {Button, Map} from '../../widgets';


const styles = require('./HomePage.scss');

export const HomePage: FunctionComponent = () => {

    const [isFluid, setIsFluid] = useState(false);


    return (
        <Container fluid={isFluid} >

            <div data-test="clickBrand" className={styles.header} >
                <div className={styles.title} >
                    Prueba técnica - GoogleMaps API
                </div>
                <div className={styles.headerContent} >
                    <div>
                        ¿Que tipo de container quieres?
                    </div>
                    <Button text={`Container ${isFluid ? 'Normal' : 'Fluid'}`}
                            onClick={() => setIsFluid(!isFluid)} />
                </div>
            </div>

            <Row>
                <Col xs={12} sm={12} md={12} lg={8} className={styles.col} >
                    <Map id="big" />
                </Col>
                <Col xs={12} sm={12} md={12} lg={4} className={styles.col} >
                    <Map id="little" />
                </Col>
            </Row>

        </Container>
    );
}
