
import * as React from 'react';
import {FormEvent, FunctionComponent, ReactElement, useEffect, useState} from 'react';

import {GoogleMapService} from '../../../../services';
import {PredictionConfig} from '../../../../models';
import AutocompletePrediction = google.maps.places.AutocompletePrediction;


const styles = require('./AddressPlacesInput.scss');

interface AddressPlacesInputProps {

    searchConfig: PredictionConfig;
    onAddressSelect(selectedPrediction: AutocompletePrediction): void;

}

export const AddressPlacesInput: FunctionComponent<AddressPlacesInputProps> = (props) => {

    const {searchConfig, onAddressSelect} = props;

    const [inputValue, setInputValue] = useState<string>('');
    const [predictionList, setPredictionList] = useState<AutocompletePrediction[]>([]);
    const [selected, setSelected] = useState<boolean>(false);


    useEffect(() => {

        if(!selected && inputValue.length > 3) {

            GoogleMapService.getQueryPredictions(inputValue, searchConfig)
                .then((predictions) => {
                    setPredictionList(predictions);
                });

        } else {
            setPredictionList([]);
        }

    }, [inputValue]);


    const onInputChange = (event: FormEvent<HTMLInputElement>): void => {
        selected && setSelected(false);
        setInputValue(event.currentTarget.value);
    }

    const onSuggestionClick = (selectedPrediction: AutocompletePrediction): void => {

        setSelected(true);
        setInputValue(selectedPrediction.description);
        setPredictionList([]);

        onAddressSelect(selectedPrediction);
    }

    const formatSuggestions = (description: string): ReactElement => {

        const regexp = new RegExp('(^|)(' + inputValue + ')(|$)','ig');
        const newDescription = description.replace(regexp, `<b>${inputValue}</b>`);

        return (
            <span style={{textTransform: 'capitalize'}}
                  dangerouslySetInnerHTML={{__html: newDescription}} />
        );
    }

    return (
        <div className={styles.inputWrapper} >

            <input type="text"
                   placeholder="Escribe una dirección..."
                   autoComplete="whatever"
                   value={inputValue}
                   onChange={onInputChange}
                   className={styles.input} />

            {predictionList.length > 0 && (
                <ul className={styles.predictionsWrapper} >
                    {predictionList.map((item, index) => (
                        <li key={index}
                            className={styles.suggestion}
                            onClick={() => onSuggestionClick(item)} >

                            {formatSuggestions(item.description)}

                        </li>
                    ))}
                </ul>
            )}

        </div>
    );
};
