
import * as React from 'react';
import {FunctionComponent, RefObject, useEffect, useRef, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {IAppState} from '../../../../store/IAppState';

import AutocompletePrediction = google.maps.places.AutocompletePrediction;
import GoogleMap = google.maps.Map;
import {GoogleMapService} from '../../../../services';
import {Coordinates} from '../../../../models';
import {AddressPlacesInput} from '../AddressPlacesInput';
import {Dispatch} from '../../../../store/types/redux';
import {addMarkerToMap} from '../../../../store/actions/googleMapsActions';


const styles = require('./Map.scss');

interface MapProps {

    id: string;

}

export const Map: FunctionComponent<MapProps> = (props) => {

    const COORDINATES: Coordinates = {lat: 41.390205, lng: 2.154007};
    const ZOOM = 12;

    const {markers} = useSelector((state: IAppState) => state.googleMaps);

    const dispatch: Dispatch = useDispatch();
    const addMarkerToMapDispatch = (map_id: string, coords: Coordinates[]) => dispatch(addMarkerToMap(map_id, coords));

    const mapWrapper: RefObject<HTMLDivElement> = useRef(null);
    const [maps, setMaps] = useState<GoogleMap>();


    useEffect(() => {

        GoogleMapService.initMap(mapWrapper, COORDINATES, ZOOM)
            .then((resMap) => {

                setMaps(resMap);

            })
            .catch((err) => {
                console.log('ERROR', err);
            });

    }, []);

    useEffect(() => {
        loadMarkers();
    }, [maps, markers.find(m => m.map_id === props.id)]);


    const getStoredMarkers = (): Coordinates[] => {

        return markers
            .filter(m => m.map_id === props.id)
            .reduce((accumulator, currentValue) => {

                currentValue.coordinates.forEach(item => {
                    accumulator.push({
                        lat: item.lat,
                        lng: item.lng
                    });
                });

                return accumulator;

            }, [] as Coordinates[]);
    }

    const loadMarkers = (): void => {

        getStoredMarkers()
            .forEach(c => {
                // @ts-ignore
                GoogleMapService.addMarker(maps, {...c});
            });
    }

    const onAddressSelect = (suggestion: AutocompletePrediction): void => {

        GoogleMapService.getPredictionDetails(suggestion)
            .then((details) => {

                const coordinates = {
                    lat: details.geometry?.location.lat() || 0,
                    lng: details.geometry?.location.lng() || 0
                };

                addMarkerToMapDispatch(props.id, [coordinates]);

            })
            .catch((err) => {
                console.log('ERROR', err);
            });
    }

    return (
        <div className={styles.container} >

            <div className={styles.addressWrapper} >
                <AddressPlacesInput searchConfig={{coordinates: COORDINATES, radius: 20000}}
                                    onAddressSelect={onAddressSelect} />
            </div>

            <div className={styles.mapWrapper} ref={mapWrapper} />

        </div>
    );
};
