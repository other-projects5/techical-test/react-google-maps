
import * as React from 'react'
import {FunctionComponent, ButtonHTMLAttributes} from 'react';


const styles = require('./Button.scss');

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
    text: string;
}

export const Button: FunctionComponent<ButtonProps> = (props) => {

    const {text, ...rest} = props;

    return (
        <button className={styles.button} {...rest} >
            {text}
        </button>
    );
}
