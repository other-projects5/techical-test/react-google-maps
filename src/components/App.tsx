
import * as React from 'react';
import {Component, ReactNode} from 'react';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

import {store} from '../store/store';
import {Routes} from './routes';


export class App extends Component {
    
    public render(): ReactNode {
        
        return (
            <Provider store={store} >

                <BrowserRouter>

                    <Routes />
                    
                </BrowserRouter>

            </Provider>
        );
    }
}