
import {googleMapsReducer} from '../../../../src/store/reducers/googleMapsReducer';
import {initialState} from '../../../../src/store/initialState';
import {ADD_MARKER} from '../../../../src/store/types/googleMapsTypes';


describe('Reducers - googleMapsReducer', () => {

    const state = initialState.googleMaps;


    test('Should return initialState', () => {

        // Mocks
        const actionMock = {
            type: 'invalidType',
            payload: {}
        };

        // Simulation
        const res = googleMapsReducer(state, actionMock);

        // Assertion
        expect(res).toEqual(state);
    });

    test('Add marker', () => {

        // Mocks
        const payload = [{
            map_id: 'big',
            coordinates: [{lat: 1, lng: 1}]
        }];

        const actionMock = {
            type: ADD_MARKER,
            payload
        };

        // Simulation
        const res = googleMapsReducer(state, actionMock);

        // Assertion
        expect(res).toEqual({
            markers: payload
        });
    });

});
