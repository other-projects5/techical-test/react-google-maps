
import createMockStore, {MockStoreCreator, MockStoreEnhanced} from 'redux-mock-store';
import thunk from 'redux-thunk';

import {Dispatch} from '../../../../src/store/types';
import {ADD_MARKER} from '../../../../src/store/types/googleMapsTypes';
import {initialState} from '../../../../src/store/initialState';
import {IAppState} from '../../../../src/store/IAppState';
import {addMarkerToMap} from '../../../../src/store/actions/googleMapsActions';


const middlewares: any = [thunk];
const mockStore: MockStoreCreator<IAppState, Dispatch> = createMockStore<IAppState, Dispatch>(middlewares);


describe('Action creators - googleMapsActions', () => {

    let store: MockStoreEnhanced<IAppState, Dispatch>;


    beforeEach(() => {
        store = mockStore(initialState);
    });


    test('addMarkerToMap', () => {

        // Mocks
        const markerMock = {
            map_id: 'big',
            coordinates: [{lat: 1, lng: 1}]
        };

        // Simulation
        store.dispatch(addMarkerToMap(markerMock.map_id, markerMock.coordinates));
        const actions = store.getActions();

        // Assertions
        expect(actions[0]).toEqual({
            type: ADD_MARKER,
            payload: [markerMock]
        });

    });
});
