
import * as React from 'react';
import renderer from 'react-test-renderer';

import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {AddressPlacesInput} from '../../../../../../src/components/widgets';

configure({adapter: new Adapter()});


describe('AddressPlacesInput', () => {

    test('Should render AddressPlacesInput and checkSnapshot', () => {

        const wrapper = renderer.create(
            <AddressPlacesInput searchConfig={{coordinates: {lat: 41.390205, lng: 2.154007}, radius: 20000}}
                                onAddressSelect={jest.fn()} />
            );

        let tree = wrapper.toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('Write and select an option', () => {

        // Mock
        const onAddressSelect = jest.fn();
        const wrapper = shallow(
            <AddressPlacesInput searchConfig={{coordinates: {lat: 41.390205, lng: 2.154007}, radius: 20000}}
                                onAddressSelect={onAddressSelect} />
            );

        // Simulate
        wrapper.find('input').simulate('change', {currentTarget: {value: 'arc de tr'}});

        setTimeout(() => {
            wrapper.update();
            wrapper.find('li').at(0).simulate('click');

            // Assertion
            expect(wrapper.find('input').prop('value')).toBe('arc de tr');
            expect(onAddressSelect.mock.calls.length).toBe(1);
        }, 3000);


    });
});
