
import * as React from 'react';
import renderer from 'react-test-renderer';

import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {Button} from '../../../../../src/components/widgets';


configure({adapter: new Adapter()});

describe('Button', () => {

    test('Should render Button and checkSnapshot', () => {

        const wrapper = renderer.create(<Button text="Testing" />);

        let tree = wrapper.toJSON();

        expect(tree).toMatchSnapshot();
    });

    test('Check button text', () => {

        // Mock
        const wrapper = shallow(<Button text="Testing" />);

        // Simulation
        const text = wrapper.find('button').text();

        // Assertion
        expect(text).toBe('Testing');
    });

    test('Simulate click event', () => {

        // Mock
        const onButtonClick = jest.fn();
        const wrapper = shallow(<Button text="Testing" onClick={onButtonClick} />);

        // Simulation
        wrapper.find('button').simulate('click');

        // Assertion
        expect(onButtonClick.mock.calls.length).toBe(1);
    });

});
