
import * as React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';

import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {createStoryStore} from '../../../../utils/createStore';
import {HomePage} from '../../../../../src/components/pages';


configure({adapter: new Adapter()});

describe('HomePage', () => {

    const component = (
        <Provider store={createStoryStore()} >
            <HomePage />
        </Provider>
    );

    test('Should render HomePage and checkSnapshot', () => {

        const wrapper = renderer.create(component);

        let tree = wrapper.toJSON();

        expect(tree).toMatchSnapshot();
    });

});
